// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "Math/Vector2D.h"
#include "Tank.generated.h"


/**
 * 
 */
UCLASS()
class TOONTANKS_API ATank : public ABasePawn
{
	GENERATED_BODY()
private:
	UPROPERTY(VisibleAnywhere, Category="Component", meta=(AllowPrivateAccess=true))
	class USpringArmComponent* SpringArmComponent;
	UPROPERTY(VisibleAnywhere, Category="Component", meta=(AllowPrivateAccess=true))
	class UCameraComponent* CameraComponent;
	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess=true))
	int MoveSpeed = 200;
	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess=true))
	int RotateSpeed = 10;
	UFUNCTION(BlueprintCallable, meta=(AllowPrivateAccess=true))
	void Move(FVector MoveInput);
	UFUNCTION(BlueprintCallable, meta=(AllowPrivateAccess=true))
	virtual void Fire() override;
	APlayerController* TankPlayerController;
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
public:
	ATank();
	APlayerController* GetTankPlayerController() const { return TankPlayerController; }
	virtual void HandleDestruction() override;

	bool isAlive;
};
