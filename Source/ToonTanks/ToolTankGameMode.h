// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ToolTankGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TOONTANKS_API AToolTankGameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	void ActorDied(AActor* DeadActor);
protected:
	void BeginPlay() override;
	UFUNCTION(BlueprintImplementableEvent)
	void StartGame();

	UFUNCTION(BlueprintImplementableEvent)
	void GameOver(bool PlayerWon);
private:

	class ATank* PlayerTank;
	class AToonTankPlayerController* PlayerControllerRef;
	float StartDelay = 3.f;

	void HandleGameStart();
	int TargetTowers = 0;

	int GetTargetTowerCount();
};
