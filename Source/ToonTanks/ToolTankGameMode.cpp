// Fill out your copyright notice in the Description page of Project Settings.


#include "ToolTankGameMode.h"

#include "Tank.h"
#include "ToonTankPlayerController.h"
#include "Tower.h"
#include "Kismet/GameplayStatics.h"

void AToolTankGameMode::ActorDied(AActor* DeadActor) {
	//UE_LOG(LogTemp, Warning, TEXT("Actor Died: %s"), *DeadActor->GetName());
	if (DeadActor == nullptr) {
		return;
	}
	if (ATank* Tank = Cast<ATank>(DeadActor)) {
		Tank->HandleDestruction();
		if (PlayerControllerRef) {
			PlayerControllerRef->SetPlayerEnabledState(false);
		}
		GameOver(false);
	} else if (	ATower* Tower = Cast<ATower>(DeadActor)) {
		Tower->HandleDestruction();
		--TargetTowers;
		if (TargetTowers == 0) {
			GameOver(true);
		}
	}
}

void AToolTankGameMode::BeginPlay() {
	Super::BeginPlay();
	HandleGameStart();
}

void AToolTankGameMode::HandleGameStart() {
	TargetTowers = GetTargetTowerCount();
	PlayerTank = Cast<ATank>(
		UGameplayStatics::GetPlayerPawn(this, 0));
	PlayerControllerRef = Cast<AToonTankPlayerController>(
		UGameplayStatics::GetPlayerController(this, 0));
	StartGame();
	if (PlayerControllerRef) {
		PlayerControllerRef->SetPlayerEnabledState(false);
		FTimerHandle PlayerEnableHandle;
		const FTimerDelegate PlayerEnableDelegate =
			FTimerDelegate::CreateUObject(
				PlayerControllerRef,
				&AToonTankPlayerController::SetPlayerEnabledState,
				true);
		GetWorld()->GetTimerManager().SetTimer(
			PlayerEnableHandle, PlayerEnableDelegate,
			StartDelay, false);
	}
}

int AToolTankGameMode::GetTargetTowerCount() {
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(this, ATower::StaticClass(),
		FoundActors);
	return FoundActors.Num();
}
