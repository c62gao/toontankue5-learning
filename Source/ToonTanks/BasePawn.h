// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BasePawn.generated.h"


class UInputMappingContext;

UCLASS()
class TOONTANKS_API ABasePawn : public APawn {
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABasePawn();
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Some Category")
	int32 VisibleAnywhereInt = 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Some Category")
	int32 EditAnywhere = 1;
	UPROPERTY(VisibleInstanceOnly)
	int32 VisibleInstanceOnly = 1;
	UPROPERTY(VisibleDefaultsOnly)
	int32 VisibleDefaultOnly = 5;
	UPROPERTY(EditInstanceOnly)
	int32 EditInstanceOnly = 1;
	UPROPERTY(EditDefaultsOnly)
	int32 EditDefaultOnly = 1;
	UPROPERTY(EditAnywhere)
	float TurretRotateSpeed = 5.f;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Fire();
	void RotateTurret(FVector LookAtPoint);
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess=true))
	class UCapsuleComponent* CapsuleComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess=true))
	UStaticMeshComponent* BaseMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess=true))
	UStaticMeshComponent* TurretMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess=true))
	USceneComponent* ProjectTileSpawnPoint;

	UPROPERTY(EditDefaultsOnly, Category="Combat")
	// ReSharper disable once UnrealHeaderToolError
	TSubclassOf<class AProjectile> ProjectileClass;
	UPROPERTY(EditAnywhere, Category="Combat")
	class UParticleSystem* DeathParticle;
	UPROPERTY(EditAnywhere, Category="Combat")
	class USoundBase* DeathSound;

	UPROPERTY(EditAnywhere, Category="Combat")
	TSubclassOf<class UCameraShakeBase> DeathCameraShakeClass;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;
	virtual void HandleDestruction();
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputMappingContext *InputContext;
};
