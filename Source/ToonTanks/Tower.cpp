// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Tank.h"
#include "Kismet/GameplayStatics.h"

void ATower::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);
	if(InFireRange()) {
		RotateTurret(Tank->GetActorLocation());
	}
}



void ATower::BeginPlay() {
	Super::BeginPlay();
	Tank = Cast<ATank>(UGameplayStatics::GetPlayerPawn(this, 0));
	GetWorldTimerManager().SetTimer(FireRateTimerHandle, this, &ATower::CheckFireCondition,
	FireRate, true);
}

void ATower::Fire() {
	Super::Fire();
}

void ATower::CheckFireCondition() {
	if (InFireRange() && Tank->isAlive) {
		Fire();
	}
}

bool ATower::InFireRange() {
	if (!Tank) {
		return false;
	}
	float Distance = FVector::Dist(this->GetActorLocation(),
	                               Tank->GetActorLocation());
	return  Distance <= FireRange;
}
void ATower::HandleDestruction() {
	Super::HandleDestruction();
	Destroy();
}
