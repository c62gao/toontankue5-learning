// Fill out your copyright notice in the Description page of Project Settings.


#include "Tank.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"

ATank::ATank() {
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(
		TEXT("Spring Arm"));
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	SpringArmComponent->SetupAttachment(RootComponent);
	CameraComponent->SetupAttachment(SpringArmComponent);
}

void ATank::HandleDestruction() {
	Super::HandleDestruction();
	SetActorHiddenInGame(true);
	SetActorTickEnabled(false);
	isAlive = false;
}

void ATank::Move(FVector MoveInput) {
	FVector DelataLocation = FVector::ZeroVector;
	FRotator Rotator = FRotator::ZeroRotator;
	DelataLocation.X = MoveInput.X *
		UGameplayStatics::GetWorldDeltaSeconds(this) * MoveSpeed;
	Rotator.Yaw = MoveInput.Y * UGameplayStatics::GetWorldDeltaSeconds(this) *
		RotateSpeed;
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *MoveInput.ToString());
	AddActorLocalOffset(DelataLocation, true);
	AddActorLocalRotation(Rotator, true);
}

void ATank::Fire() {
	Super::Fire();
}


void ATank::BeginPlay() {
	Super::BeginPlay();
	TankPlayerController = Cast<APlayerController>(GetController());
	isAlive = true;
}

void ATank::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (TankPlayerController) {
		FHitResult HitResult;
		TankPlayerController->GetHitResultUnderCursor(
			ECC_Visibility, false, HitResult);
		//DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 10.f, 12, FColor::Red, false, -1);
		RotateTurret(HitResult.ImpactPoint);
	}
}
